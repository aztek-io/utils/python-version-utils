#!/usr/bin/env python

import unittest
import version
import logging
import sys
import git
import os

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Unit Tests ########################
#######################################

class TestScript(unittest.TestCase):
    def test_fatal(self):
        print('')
        logger.info('Testing fatal()')

        # Test 1
        with self.assertRaises(SystemExit) as ut:
            version.fatal(message='Unit test exit code 7', code=7)

        self.assertEqual(
            ut.exception.code,
            7
        )

        # Test 2
        with self.assertRaises(SystemExit) as ut:
            version.fatal(message='Unit test exit code 15', code=15)

        self.assertEqual(
        ut.exception.code,
            15
        )


    def test_get_branch(self):
        print('')
        logger.info('Testing get_branch()')

        # Test 1
        global_branch = os.environ.get('CI_BUILD_REF_NAME', None)

        if not global_branch:
            with self.assertRaises(SystemExit) as ut:
                broken_repo = 'not repo object'
                version.get_branch(repo=broken_repo)

            self.assertEqual(
                ut.exception.code,
                99
            )
        else:
            logger.info('Skipping SystemExit unit test due to override variable.')

        # Test 2
        repo            = git.Repo(os.getcwd())
        branch          = os.popen('git rev-parse --abbrev-ref HEAD').read().strip()
        global_branch   = None
        logger.info('Branch: {}'.format(branch))

        if len(branch) == 40 or branch == 'HEAD':
            logger.info('Determined that he HEAD is detached.')
            branch          = os.environ.get('CI_BUILD_REF_NAME', None)
            global_branch   = branch
            logger.info('Set branch to {} for unit testing.'.format(global_branch))

            if not global_branch:
                logger.fatal('Unable to proceed since there is not an override variable that can be used.')
                sys.exit(1)

        utils_branch = version.get_branch(repo=repo, global_branch=global_branch)

        logger.info('Version Utils branch: {}'.format(utils_branch))
        logger.info('CLI git branch: {}'.format(branch))

        self.assertEqual(
            utils_branch,
            branch
        )


    def test_get_last_gat(self):
        print('')
        logger.info('Testing get_last_tag()')

        def check_tags(tags, expected_value):
            self.assertEqual(
                version.get_last_tag(tags=tags),
                expected_value
            )

        # Test 1
        tags = ['first', 'second', 'last']
        check_tags(tags, 'last')

        # Test 2
        tags = ['']
        check_tags(tags, None)

        # Test 3
        tags = ['first', '']
        check_tags(tags, 'first')


    def test_get_change_type(self):
        print('')
        logger.info('Testing get_change_type()')

        def check_messages(messages, expected_change_type):
            self.assertEqual(
                version.get_change_type(messages),
                expected_change_type
            )

        # Test 1
        messages = ['FiXeD TyPo']
        check_messages(messages, 'patch')

        # Test 1.1
        messages = ['FiXeD TyPo', 'Some minor change.']
        check_messages(messages, 'patch')

        # Test 2
        messages = ['Major change']
        check_messages(messages, 'major')

        # Test 2.1
        messages = ['Major change', 'Some minor change.']
        check_messages(messages, 'major')

        # Test 3
        messages = ['Nothing to see here.']
        check_messages(messages, 'minor')

        # Test 3.1
        messages = ['Nothing to see here.', 'Fixed Typo.']
        check_messages(messages, 'minor')


    def test_get_commits_since_merge(self):
        print('')
        logger.info('Testing get_commits_since_merge()')

        # Test 1
        self.assertEqual(
            isinstance(version.get_commits_since_merge(), list),
            True
        )


    def test_get_new_version(self):
        print('')
        logger.info('Testing get_new_version()')

        def check_version_logic(branch, last_version, expected_version):
            seed        = '1.0.0'
            tag_prefix  = 'u'

            logger.info('Branch name: {}'.format(branch))

            if branch == 'trunk':
                tag_postfix = None
            else:
                tag_postfix = '-test'


            self.assertEqual(
                version.get_new_version(
                    last_version=last_version,
                    tag_postfix=tag_postfix,
                    change_type='minor',
                    tag_prefix=tag_prefix,
                    seed=seed
                ),
                expected_version
            )


        # Test 1 last_version is None
        check_version_logic(
            branch='trunk',
            last_version=None,
            expected_version='u1.0.0'
        )

        # Test 1.1 last_version and tag_suffix is None
        check_version_logic(
            branch='anything',
            last_version=None,
            expected_version='u1.0.0-test.1'
        )

        # Test 2
        check_version_logic(
            branch='anything',
            last_version='u1.0.0-test.1',
            expected_version='u1.0.0-test.2'
        )

        # Test 3
        check_version_logic(
            branch='anything',
            last_version='u1.0.0',
            expected_version='u1.1.0-test.1'
        )


    def test_increment_version(self):
        print('')
        logger.info('Testing increment_version()')

        def check_version(last_version, change_type, expected_version):
            tag_prefix      = 'u'
            tag_postfix     = '-test'

            new_version = version.increment_version(
                last_version=last_version,
                tag_postfix=tag_postfix,
                change_type=change_type,
                tag_prefix=tag_prefix
            )

            self.assertEqual(new_version, expected_version)

        last_version = 'u1.1.1-test.1'

        # Tests 1-3
        check_version(last_version, change_type='patch', expected_version='u1.1.1-test.2')
        check_version(last_version, change_type='minor', expected_version='u1.1.1-test.2')
        check_version(last_version, change_type='major', expected_version='u1.1.1-test.2')

        last_version = 'u1.1.1'

        # Tests 4-6
        check_version(last_version, change_type='patch', expected_version='u1.1.2-test.1')
        check_version(last_version, change_type='minor', expected_version='u1.2.0-test.1')
        check_version(last_version, change_type='major', expected_version='u2.0.0-test.1')


    def test_get_version_as_list(self):
        print('')
        logger.info('Testing get_version_as_list()')

        def check_version_list(last_version, expected_major, expected_minor, expected_patch, expected_postfix_number):
            logger.info('Test Case: {}'.format(last_version))

            tag_prefix   = 'u'
            tag_postfix  = '-test'

            major, minor, patch, postfix_number = version.get_version_as_list(tag_prefix, tag_postfix, last_version)

            self.assertEqual(
                major,
                expected_major
            )

            self.assertEqual(
                minor,
                expected_minor
            )

            self.assertEqual(
                patch,
                expected_patch
            )

            self.assertEqual(
                postfix_number,
                expected_postfix_number
            )


        # Test 1
        check_version_list('u1.2.3-test.4', 1,2,3,4)

        # Test 2
        check_version_list('u1.2.3', 1,2,3,None)


if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    unittest.main()
