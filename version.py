#!/usr/bin/env python
# Written by:   Robert J.
# Email:        robert@aztek.io

import logging
import os
import sys
import git
import re
import json

import git_history

from git.exc import GitCommandError

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Global Variables ##################
#######################################

VERSION_SEED    = os.environ.get('VERSION_SEED', '0.1.0')
BRANCH          = os.environ.get('CI_BUILD_REF_NAME', None)
TAG_PREFIX      = os.environ.get('TAG_PREFIX', 'v')
UTILS_VERSION   = os.environ.get('UTILS_VERSION', None)

#######################################
### Main Function #####################
#######################################

def main():
    '''
    These version utils are focused towards a trunk based development model (https://trunkbaseddevelopment.com/).

    The exception to this is that instead of using release branches that are protected, protected git tags are used instead to reduce complexity.

    "Branches are evil" - Gary Gruver
    '''
    logger.info('Python Version Utils Version: {}')
    repo        = git.Repo(os.getcwd())
    branch      = get_branch(repo)

    tag_postfix = get_tag_postfix(branch)

    tags        = get_remote_git_tags(repo)

    last_tag    = get_last_tag(tags)
    commit_log  = get_commits_since_merge()
    change_type = get_change_type(commit_log)

    new_version = get_new_version(last_tag, tag_postfix, change_type)

    write_new_version_to_files(tag_postfix, new_version)

    logger.info('Successfully completed versioning.')


#######################################
### Generic Functions #################
#######################################

def fatal(message, code=1):
    logger.fatal(message)
    sys.exit(code)


#######################################
### Program Specific Functions ########
#######################################

def get_branch(repo, global_branch=BRANCH):
    if global_branch:
        branch  = global_branch
    else:
        try:
            branch = repo.active_branch.name
        except TypeError as e:
            fatal(e)
        except AttributeError as e:
            fatal(e, 99)

    logger.info('Current Branch: {}'.format(branch))

    repo.git.checkout(branch)

    return branch


def get_tag_postfix(branch):
    if branch in ['master', 'main', 'develop']:
        tag_postfix = None
    else:
        tag_postfix = '-beta'

    logger.info('Tag postfix: {}'.format(tag_postfix))

    return tag_postfix

def get_local_git_tags(repo):
    tstr = repo.git.tag(l=True, sort='committerdate')
    tags = str(tstr).split('\n')

    logger.debug('Tags: {}'.format(json.dumps(tags, indent=4)))

    return tags


def delete_local_git_tags(repo):
    logger.info('Removing local tags')

    for tag in get_local_git_tags(repo):
        repo.git.tag(d=tag)


def get_remote_git_tags(repo, retry=0):
    try:
        repo.git.pull(tags=True)
    except GitCommandError as e:
        logger.warn('Unable to pull git tags: {}'.format(e))
        delete_local_git_tags(repo)

        if retry < 2:
            logger.info('Retrying to pull tags.')
            get_remote_git_tags(repo, retry=retry+1)
        else:
            fatal('Unable to pull remote tags.', 3)

    return get_local_git_tags(repo)


def get_last_tag(tags):
    tags = [
        tag for tag in tags if len(tag) > 0
    ]

    logger.debug('Tags: {}'.format(tags))

    if len(tags) > 0:
        matching_tag = tags[-1]
    else:
        matching_tag = None

    logger.info('Tag Match: {}'.format(matching_tag))

    return matching_tag


def get_change_type(messages):
    change_type = 'minor'
    patch_words = ["typo", "bug", "fix", "corrected", "loglevel"]

    for message in messages:
        if message == messages[0]:
            if re.match('|'.join(patch_words), message.lower()):
                change_type = 'patch'

        if re.match('major', message.lower()):
            change_type = 'major'

        logger.info('Determined that this is a {} change: {}'.format(change_type, message))

        if change_type is not 'minor':
            break

    return change_type


def get_commits_since_merge():
    commits = git_history.main()

    commits_since_merge = []

    for commit in commits:
        try:
            if commit['merge']:
                break
        except KeyError as e:
            logger.debug(e)

        commits_since_merge.append(commit)

    logger.debug('Commits since last merge: {}'.format(commits_since_merge))

    messages = [
        commit['message'] for commit in commits_since_merge
    ]

    return messages


def get_new_version(last_version, tag_postfix, change_type, seed=VERSION_SEED, tag_prefix=TAG_PREFIX):
    if not last_version and not tag_postfix:
        # I didn't originally anticipate that anyone would run this on trunk fist go.
        tag_suffix  = ""
        new_version = tag_prefix + seed + tag_suffix
    elif not last_version:
        new_version = tag_prefix + seed + tag_postfix + '.1'
    elif not tag_postfix:
        new_version = last_version.split('-')[0]
    else:
        new_version = increment_version(last_version, tag_prefix, tag_postfix, change_type)

    logger.info('New version: {}'.format(new_version))

    return new_version


def increment_version(last_version, tag_prefix, tag_postfix, change_type):
    major, minor, patch, postfix_number = get_version_as_list(tag_prefix, tag_postfix, last_version)
    logger.info('Change Type: {}'.format(change_type))

    if not postfix_number:
        if change_type == 'major':
            major += 1
            minor = 0
            patch = 0
        elif change_type == 'minor':
            minor += 1
            patch = 0
        elif change_type == 'patch':
            patch += 1
        else:
            fatal('Unhandled change type: {}'.format(change_type))

        logger.info('Incremented {}'.format(change_type))
        postfix_number = 1
    else:
        postfix_number += 1
        logger.info('Incremented pre-release.')

    new_version = '{}.{}.{}'.format(major, minor, patch)

    logger.info('New Symantec versioning: {}'.format(new_version))

    return '{}{}{}.{}'.format(tag_prefix, new_version, tag_postfix, postfix_number)


def get_version_as_list(tag_prefix, tag_postfix, last_version):
    trim_pattern    = '|'.join([tag_prefix, tag_postfix])
    version         = re.sub(trim_pattern, '', last_version)
    version_list    = version.split('.')

    logger.debug('Version list: {}'.format(version_list))

    major           = int(version_list[0])
    minor           = int(version_list[1])
    patch           = int(version_list[2])

    postfix_number  = None

    if len(version_list) == 4:
        postfix_number  = int(version_list[3])

    return major, minor, patch, postfix_number


def write_new_version_to_files(tag_postfix, version, tag_prefix=TAG_PREFIX):
    version_json = {
        'versionPostfix': tag_postfix,
        'postfixIncrement': version.split('.')[-1],
        'versionPrefix': tag_prefix,
        'fullVersion': version,
        'symantecVersion': re.search(r'(\d+\.){2}\d+', version).group(0)
    }

    write_to_file('tag_version.json', json.dumps(version_json, indent=4))
    write_to_file('tag_version.txt',  version)

    logger.info('Version json: {}'.format(json.dumps(version_json, sort_keys=True, indent=4)))

    return version_json


def write_to_file(filename, content):
    f = open(filename, 'w')
    f.write(content + '\n')
    logger.info('Wrote to {}.'.format(f.name))
    f.close()

    logger.debug('File handles closed.')


#######################################
### Execution #########################
#######################################

if __name__ == "__main__":
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    main()
