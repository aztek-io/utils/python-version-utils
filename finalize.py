#!/usr/bin/env python
# Written by:   Robert J.
# Email:        robert@aztek.io

import logging
import os
import sys
import git
import re
import json

from git.exc import GitCommandError

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Global Variables ##################
#######################################

USERNAME = os.environ.get('GIT_USERNAME', 'Robert J.')
EMAIL    = os.environ.get('EMAIL', 'no-reply@aztek.io')
SSH_KEY  = os.environ['GITLAB_SSH_KEY']
DOMAIN   = os.environ['GITLAB_DOMAIN']

#######################################
### Main Function #####################
#######################################

def main():
    '''
    These version utils are focused towards a trunk based development model (https://trunkbaseddevelopment.com/).

    This script will push the git tag to origin using an ssh key.

    "Branches are evil" - Gary Gruver
    '''
    logger.info('Python Version Utils Version: {}')

    repo    = git.Repo(os.getcwd())
    version = get_version_from_file()

    configure_username(repo)
    configure_ssh_key()

    finalize_version(repo, version['fullVersion'])

    logger.info('Successfully completed versioning.')


#######################################
### Generic Functions #################
#######################################

def fatal(message, code=1):
    logger.fatal(message)
    sys.exit(code)


#######################################
### Program Specific Functions ########
#######################################

def configure_username(repo, user_name=USERNAME, email=EMAIL):
    logger.info('Setting username ({user}) and email ({email})'.format(user=user_name, email=email))

    cw = repo.config_writer()

    cw.set_value("user", "name", user_name)
    cw.set_value("user", "email", email)

    cw.release()

    logger.info('Done configuring user and email.')


def get_version_from_file(filename='tag_version.json'):
    try:
        with open(filename, 'r') as version_file:
            version = json.load(version_file)
    except FileNotFoundError as e:
        error = 'version.py must be ran before executing finalize.py.\n{}'.format(e)
        fatal(message=error, code=3)

    version_file.close()

    logger.info(
        json.dumps(version, indent=4)
    )

    return version


def configure_ssh_key(ssh_key=SSH_KEY):
    home_dir        = os.environ['HOME']
    ssh_dir         = os.path.join(home_dir, '.ssh')
    ssh_config      = os.path.join(ssh_dir, 'config')
    ssh_key_file    = os.path.join(ssh_dir, 'git_ssh_key')

    if not os.path.isdir(ssh_dir):
        logger.info('Creating directory: {}'.format(ssh_dir))
        os.makedirs(ssh_dir)

    ssh_config_settings = '''
    Host {domain}
      IdentityFile {key_file}
      StrictHostKeyChecking no
    '''.format(domain=DOMAIN, key_file=ssh_key_file)

    with open(ssh_config, 'w') as sc:
        sc.write(ssh_config_settings)

    sc.close()

    with open(ssh_key_file, 'w') as skf:
        skf.write(ssh_key + '\n')

    skf.close()

    os.chmod(ssh_key_file, 0o600)


def finalize_version(repo, version):
    logger.info("Tagging version: {}".format(version))

    try:
        repo.git.tag(version)
    except GitCommandError as e:
        logger.warn('Tag seems to already exist: {}'.format(e))
        return

    remote_url = repo.remotes.origin.url

    logger.info('Remote URL: {}'.format(remote_url))

    if re.match('^http', remote_url):
        logger.info('Transforming URL.')

        transformed_url = 'git@{domain}:{project}'.format(domain=DOMAIN, project='/'.join(remote_url.split('/')[3:]))
        logger.info('Transformed URL: {}'.format(transformed_url))

        logger.info('Setting new origin.')
        repo.remotes.origin.set_url(transformed_url)

    try:
        repo.git.push(tags=True)
    except GitCommandError as e:
        fatal(e, 129)


#######################################
### Execution #########################
#######################################

if __name__ == "__main__":
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    main()
