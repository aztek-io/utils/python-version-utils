#!/usr/bin/env python
# Written by:   Robert J.
# Email:        robert@aztek.io

import json
import logging
import os
import sys
import git
import re

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Main Function #####################
#######################################

def main():
    repo    = git.Repo(os.getcwd())
    commits = get_commits(repo)

    logger.debug(json.dumps(commits, indent=4))

    return commits


def get_commits(repo):
    commits = str(repo.git.log()).split('\n\ncommit ')

    commit_list = []

    for commit in commits:
        if commit == commits[0]:
            commit = commit.strip('^commit ')

        logger.debug('Commit: {}'.format([commit]))

        hash = commit[:40]
        logger.debug('Hash: {}'.format(hash))

        merge_regex     = 'Merge:.*[^\n]\n'
        author_regex    = 'Author:.*[^\n]\n'
        date_regex      = 'Date:.*[^\n]\n\n'

        merge   = search_commit(merge_regex, commit)
        author  = search_commit(author_regex, commit)
        date    = search_commit(date_regex, commit)

        message = commit.replace(hash + '\n', '')

        for regex in [merge_regex, author_regex, date_regex]:
            message = re.sub(regex, '', message).strip()

        logger.debug('Message: {}'.format(message))

        commit_dict = {
            "hash": hash,
            "author": author,
            "date": date,
            "message": message
        }

        if merge:
            commit_dict.update({"merge": merge})

        logger.debug(json.dumps(commit_dict, indent=4))

        commit_list.append(commit_dict)

    return commit_list


def search_commit(regex, commit):
    search = re.search(regex, commit)
    if search:
        match = search.group(0)
        logger.debug('REGEX Match for {} found: {}'.format([regex], match))
        value_list = match.strip('\n').split(':')[1:]

        logger.debug('Value List: {}'.format(value_list))
        value = ':'.join(value_list).strip()
    else:
        value = None

    logger.debug('Match: {}'.format(value))

    return value


#######################################
### Execution #########################
#######################################

if __name__ == "__main__":
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    main()
