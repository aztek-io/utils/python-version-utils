FROM python:3.7.4-alpine

WORKDIR /app

COPY ["requirements.txt", "."]

# hadolint ignore=DL3018
RUN apk add --no-cache git openssh && \
        pip install --no-cache-dir -r requirements.txt

COPY ["git_history.py", "."]
COPY ["version.py", "."]
COPY ["finalize.py", "."]

ARG UTILS_VERSION

RUN ln -s '/app/git_history.py' '/bin/git_history' && \
        ln -s '/app/version.py' '/bin/version' && \
        ln -s '/app/finalize.py' '/bin/finalize'

ENTRYPOINT ["/app/version.py"]
